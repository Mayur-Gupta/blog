Instruction before start project
1. Install python 2.6 on your system
2. Then install virtual environment:- sudo pip install virtualenv 
3. Then create:- virtualenv blog 
4. cd to blog virtualenv
6. Activate using:- source bin/activate
7. Then git clone 'https'
8. cd to repo /Blog
9. pip install requirements.txt
10. cd /src
11. runserver: python manage.py runserver
12. Go to  Home Page Localhost:8000
---------------- Very basic UI -------------------
This ui only for show all functionaliy of this blog.
1 how this blog looks, you can add, delete, update 
2 Register and login user
3 Basic user info like Email and Name
4 comment below each blog. 
5 like/share blog on facebook 
---------------------------- Features in this blog -------------------------------
[1 - Home Page Localhost:8000]
[2 - Admin Page Localhost:8000/admin]
[3 - Render HTML and Markdown]
[4 - Implement Django Pagedown for Stack Overflow style Markdown]
[5 - Responsive Image inside of Post Markdwon Content]
[6 - Render Markdown with Django]
[7 - Truncate and Django Template Tags]
[8 - Dynamic Preview of Form Data]
[9 - Django Crispy Forms]
[10 - Bootstrap Input Groups]
[11 - Model Managers & Instance Methods]
[12 - Create Comments]
[13 - Reply to Comments]
[14 - jQuery fadeToggle for Comment Replies]
[15 - Comment Thread]
[16 - Count Words & Get Read Time with Python]
[17 - Blog Post Read Time in Django]
[18 - Delete View with Confirmation Page]
[19 - Delete Permissions & Http Status Codes]
[20 - Basic User Login, Registration, Logout]
[21 - User Login & Form Validation]
[22 - User Register Form & View]
[23 - User Login Required]
[24 - Breadcrumb Navigation]
--------------------- Hiden features -----------------------
[1 On blog details page you can type after url edit, you can simply edit post your post]
for example: http://localhost:8000/second-commit/
enter edit like
http://localhost:8000/second-commit/edit
[2 If you are login simple after hostname type 'create']
http://localhost:8000/create
you can add new post from there.